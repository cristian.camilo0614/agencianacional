PRUEBA DE DESARROLLADOR FRONTED

Nombre del proyecto: AgenciaDigital

Framework: Este proyecto se generó con Angular CLI versión 9.1.4.

Servidor: Ejecutar ng serve para un servidor de desarrollo. Navega a http://localhost:4200/

Librerias: Boostrap, CDN GOV.CO

Repositorio: https://gitlab.com/cristian.camilo0614/agencianacioal


Las Secciones de diseño que se implementaron fueron “infórmate” y “Otros temas de interés”, para implementar dichas secciones se debió de crear dos .json que fueron ubicados dentro de la carpeta de src/assets/data. los cuales contendrán la información de cada sección, para acceder a esta información se crearon dos servicios que se encuentran dentro de la carpeta “src/app/services. Estos servicios tendrán que  consumir su propio .json, teniendo en cuenta asi el principio de responsabilidad única, esta información retornara en sus respectivas cards en el momento que renderice el programa, para que pueda asi ser vista por el usuario. 
Se crearon 4 componentes “header”, “footer”,”body”,”home”, los cuales se separaron de acuerdo a su funcionalidad, logrando así, una alta cohesión y bajo acoplamiento entre sus diferentes partes. Haciendo el código mas limpio escalable y fácil de entender.

Crstian Camilo Diaz.

	
