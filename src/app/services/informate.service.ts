import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})


/**
 * Este servicio Se encarga de el consumo de la informarcion de un Json   
 * para mostrar la informacion de la seccion informate
 
 * @author Cristian Camilo Diaz <cristia.camilo0614@gmail.com>
 * @version 2020-10-31
 */

export class InformateService {

  

  constructor(private http : HttpClient) {
   
  }

  
    /**
    * Metodo que permite obtener la informacion del Json de para 
    * la seccion Informate. 
    */

  getInformate(){
    return this.http.get(`./assets/data/informate.json`);
  }

 

}
   


