import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})

/**
 * Este servicio Se encarga de el consumo de la informarcion de un Json   
 * para mostrar la informacion de la seccion interes.
 
 * @author Cristian Camilo Diaz <cristia.camilo0614@gmail.com>
 * @version 2020-10-31
 */

export class InteresService {

  constructor(private http: HttpClient) { }

    /**
    * Metodo que permite obtener la informacion del Json para la
    * seccion de interes. 
    */

  getInteres(){
    return this.http.get(`./assets/data/interes.json`);
  }

}
