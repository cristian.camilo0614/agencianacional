import { Component, OnInit } from '@angular/core';
import { InformateService } from '../../services/informate.service';

import { InteresService } from '../../services/interes.service';


/**
 * Esta clase se encarga de la logica de negocion
 * @author Cristian Camilo Diaz <cristia.camilo0614@gmail.com>
 * @version 2020-10-31
 */


@Component({
  selector: 'app-body',
  templateUrl: './body.component.html',
  styleUrls: ['./body.component.css']
})
export class BodyComponent implements OnInit {

  informacion=[];
  interess=[];

  respuesta:any;
  respu:any;

  constructor(private infor: InformateService,
    private inte: InteresService ) { }

  ngOnInit(){

  /**
 * al renderizar se inicializa estos metodos
 * para que se muestre la informacion inmediatamente
 * 
 */
    this.getInfortame();
    this.getInteres();
  }

  
/**
 * Metodo que consume el servicio de informacion
 * mostrando asi la informacion requerida
 * 
 */

getInfortame(){
  this.infor.getInformate()
  .subscribe(resp=>{

    this.respuesta=resp;
    this.informacion=this.respuesta.resp;
    
    
    console.log(this.respuesta);

  });
}

/**
 * Metodo que consume el servicio de interes
 * mostrando asi la informacion requerida
 * 
 */


getInteres(){
  this.inte.getInteres()
  .subscribe(resp=>{

    this.respu=resp;
    this.interess=this.respu.resp;
    
    
    console.log(this.respu);

  });
}


}


